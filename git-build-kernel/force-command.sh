#!/usr/bin/env bash

if [[ -z "$SSH_ORIGINAL_COMMAND" ]]; then
  exit 1;
fi

declare -i argc=0
declare -a argv
for arg in $SSH_ORIGINAL_COMMAND; do
  argv[$argc]="$arg"
  argc=$((argc + 1))
done

case "${argv[0]}" in
git-*) exec /usr/bin/git-shell -c "${SSH_ORIGINAL_COMMAND}" ;;
scp)
  for arg in "${argv[@]}"; do
    [[ "$arg" = "-f" ]] || continue;
    exec "${argv[@]}";
  done ;;
esac

echo "fatal: unrecognized command '${SSH_ORIGINAL_COMMAND}'" >&2
exit 128
