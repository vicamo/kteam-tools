variables:
  CHROOT_SETUP_STAGING_PREFIX: "${CI_REGISTRY_IMAGE}/staging:${CI_PIPELINE_ID}-chroot-"
  CHROOT_SETUP_TAG_PREFIX: "${CI_REGISTRY_IMAGE}/chroots:"

.chroot-setup-build-base: &chroot-setup-build-base
  stage: prerequisites
  image: docker:git
  services:
    - docker:dind
  needs: []
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
  script:
    - |
      export JOB_SERIES=$(echo "${CI_JOB_NAME}" | cut -d: -f4)
      export TAG="${CHROOT_SETUP_STAGING_PREFIX}base-${JOB_SERIES}";
    - docker build --build-arg SERIES="${JOB_SERIES}" --tag "${TAG}" chroot-setup
    - docker push "${TAG}"
    - if [ -n "${DEFAULT_IMAGE}" ]; then
        docker tag "${TAG}" "${TAG%-*}";
        docker push "${TAG%-*}";
      fi
  rules:
    - if: $CI_MERGE_REQUEST_IID
      changes:
        - chroot-setup/*
        - libs/*
    - if: $CI_MERGE_REQUEST_IID == null

chroot-setup:build:base:bionic:
  extends: .chroot-setup-build-base
  variables:
    DEFAULT_IMAGE: "true"

chroot-setup:build:base:focal:
  extends: .chroot-setup-build-base

.chroot-setup-build: &chroot-setup-build
  stage: build
  image: docker:git
  services:
    - docker:dind
  needs:
    # So far only Bionic can build other chroots.
    - chroot-setup:build:base:bionic
  variables:
    # JOB_MAKE_CHROOT_ARGS:
    JOB_BASE_SERIES: "bionic"
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
    - apk update
    - apk add xz
  script:
    - |
      export JOB_SERIES=$(echo "${CI_JOB_NAME}" | cut -d: -f3)
      export JOB_ARCH=$(echo "${CI_JOB_NAME}" | cut -d: -f4)
      export JOB_FLAVOR="${JOB_SERIES}-${JOB_ARCH}"
      export ARTIFACTS_DIR="output/jobs/${CI_JOB_NAME}"
    - mkdir -p "${ARTIFACTS_DIR}"

    - mkdir "${CI_BUILDS_DIR}/output"
    # add --privileged to mount proc, etc.
    - docker run --rm --detach --privileged
          --volume "${CI_PROJECT_DIR}:${CI_PROJECT_DIR}"
          --volume "${CI_BUILDS_DIR}/output:/usr3/chroots"
          --workdir "${CI_PROJECT_DIR}"
          --name base
          "${CHROOT_SETUP_STAGING_PREFIX}base-${JOB_BASE_SERIES}"
          sh -c 'while true; do sleep 1000; done'
    - docker exec base apt-get update --quiet
    - docker exec base dpkg -l | grep ^ii | awk '{print $2}' > base-packages.before
    - docker exec base ./chroot-setup/make_chroot "${JOB_SERIES}" "${JOB_ARCH}" ${JOB_MAKE_CHROOT_ARGS}
    # Downloaded apt lists take up to 100MB space
    - docker exec --workdir=/ base schroot -c "${JOB_FLAVOR}" -- sh -c 'rm -rf /var/lib/apt/lists/*_dists_*'
    - docker exec --workdir=/ base schroot -c "${JOB_FLAVOR}" -- du -sh /var/cache/apt /var/lib/apt
    # Orphaned font package takes 100MB space
    - docker exec --workdir=/ base schroot -c "${JOB_FLAVOR}" --
          sh -c 'if dpkg-query --status fonts-noto-cjk >/dev/null; then apt-get purge --yes fonts-noto-cjk; fi'
    - docker exec base cp "/etc/schroot/chroot.d/${JOB_FLAVOR}" "/usr3/chroots/${JOB_FLAVOR}"
    - docker exec base dpkg -l | grep ^ii | awk '{print $2}' > "${ARTIFACTS_DIR}"/manifest
    - diff -Nu base-packages.before "${ARTIFACTS_DIR}"/manifest
    - export TAG="${CHROOT_SETUP_STAGING_PREFIX}${JOB_FLAVOR}"
    - tar -C "${CI_BUILDS_DIR}/output/${JOB_FLAVOR}" -cpf - . |
          docker import --change 'CMD ["/bin/sh"]' - "${TAG}"
    - docker run --rm "${TAG}" apt-get update
    - docker push "${TAG}"
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - "output/jobs/${CI_JOB_NAME}"
  rules:
    - if: $CI_MERGE_REQUEST_IID
      changes:
        - chroot-setup/*
        - libs/*
    - if: $CI_MERGE_REQUEST_IID == null

chroot-setup:build:precise:amd64:
  extends: .chroot-setup-build
  variables:
    JOB_MAKE_CHROOT_ARGS: --keyring=/usr/share/keyrings/ubuntu-archive-removed-keys.gpg

chroot-setup:build:precise:i386:
  extends: .chroot-setup-build
  variables:
    JOB_MAKE_CHROOT_ARGS: --keyring=/usr/share/keyrings/ubuntu-archive-removed-keys.gpg

chroot-setup:build:trusty:amd64:
  extends: .chroot-setup-build

chroot-setup:build:trusty:i386:
  extends: .chroot-setup-build

chroot-setup:build:xenial:amd64:
  extends: .chroot-setup-build

chroot-setup:build:xenial:i386:
  extends: .chroot-setup-build

chroot-setup:build:bionic:amd64:
  extends: .chroot-setup-build

chroot-setup:build:bionic:i386:
  extends: .chroot-setup-build

chroot-setup:build:focal:amd64:
  extends: .chroot-setup-build

chroot-setup:build:groovy:amd64:
  extends: .chroot-setup-build

chroot-setup:build:hirsute:amd64:
  extends: .chroot-setup-build

.chroot-setup-test: &chroot-setup-test
  stage: test
  image: docker:git
  services:
    - docker:dind
  variables:
    GIT_STRATEGY: none
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
  script:
    - |
      export JOB_BASE_SERIES=$(echo "${CI_JOB_NAME}" | cut -d: -f3)
      export JOB_SERIES=$(echo "${CI_JOB_NAME}" | cut -d: -f4)
      export JOB_ARCH=$(echo "${CI_JOB_NAME}" | cut -d: -f5)
      export JOB_FLAVOR="${JOB_SERIES}-${JOB_ARCH}"

    - mkdir -p "${CI_BUILDS_DIR}/chroots/${JOB_FLAVOR}"
    - docker create --name "${JOB_FLAVOR}" "${CHROOT_SETUP_STAGING_PREFIX}${JOB_FLAVOR}"
    - docker export "${JOB_FLAVOR}" | tar -C "${CI_BUILDS_DIR}/chroots/${JOB_FLAVOR}" -xpf -
    - docker rm "${JOB_FLAVOR}"
    - docker rmi "${CHROOT_SETUP_STAGING_PREFIX}${JOB_FLAVOR}"

    # Retry git-clone for it might fail with:
    #   error: RPC failed; HTTP 500 curl 22 The requested URL returned error: 500
    - for retry in 1 2 3; do
        if git clone -o "${JOB_SERIES}" -b master --depth 1
            "https://gitlab.com/echoii/kernel/${JOB_SERIES}/linux.git"
            "${CI_BUILDS_DIR}/${JOB_SERIES}"; then
          break;
        fi;
        sleep 20;
      done

    - export CONTAINER_NAME=base
    # add --privileged to mount proc, etc.
    - docker run --rm --detach --privileged --init
          --volume "${CI_BUILDS_DIR}/chroots:/usr3/chroots"
          --volume "${CI_BUILDS_DIR}/${JOB_SERIES}:/home/build"
          --name "${CONTAINER_NAME}"
          "${CHROOT_SETUP_STAGING_PREFIX}base-${JOB_BASE_SERIES}"
          sh -c 'while true; do sleep 1000; done'
    - docker exec "${CONTAINER_NAME}"
          cp "/usr3/chroots/${JOB_FLAVOR}/${JOB_FLAVOR}" "/etc/schroot/chroot.d/${JOB_FLAVOR}"
    - docker exec "${CONTAINER_NAME}" schroot --info --chroot="${JOB_FLAVOR}"

    - docker exec "${CONTAINER_NAME}"
          schroot --chroot="${JOB_FLAVOR}" --directory=/home/build --
              fakeroot debian/rules clean
    - docker exec "${CONTAINER_NAME}"
          schroot --chroot="${JOB_FLAVOR}" --directory=/home/build --
              fakeroot debian/rules binary-headers
    - docker exec "${CONTAINER_NAME}" ls -al /home

    - docker stop "${CONTAINER_NAME}"
  rules:
    - if: $CI_MERGE_REQUEST_IID
      changes:
        - chroot-setup/*
        - libs/*
    - if: $CI_MERGE_REQUEST_IID == null

chroot-setup:test:bionic:precise:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:precise:amd64
      artifacts: false

chroot-setup:test:bionic:precise:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:precise:i386
      artifacts: false

chroot-setup:test:bionic:trusty:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:trusty:amd64
      artifacts: false

chroot-setup:test:bionic:trusty:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:trusty:i386
      artifacts: false

chroot-setup:test:bionic:xenial:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:xenial:amd64
      artifacts: false

chroot-setup:test:bionic:xenial:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:xenial:i386
      artifacts: false

chroot-setup:test:bionic:bionic:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:bionic:amd64
      artifacts: false

chroot-setup:test:bionic:bionic:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:bionic:i386
      artifacts: false

chroot-setup:test:bionic:focal:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:focal:amd64
      artifacts: false

chroot-setup:test:bionic:groovy:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:groovy:amd64
      artifacts: false

chroot-setup:test:bionic:hirsute:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:bionic
    - job: chroot-setup:build:hirsute:amd64
      artifacts: false

chroot-setup:test:focal:precise:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:precise:amd64
      artifacts: false

chroot-setup:test:focal:precise:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:precise:i386
      artifacts: false

chroot-setup:test:focal:trusty:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:trusty:amd64
      artifacts: false

chroot-setup:test:focal:trusty:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:trusty:i386
      artifacts: false

chroot-setup:test:focal:xenial:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:xenial:amd64
      artifacts: false

chroot-setup:test:focal:xenial:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:xenial:i386
      artifacts: false

chroot-setup:test:focal:bionic:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:bionic:amd64
      artifacts: false

chroot-setup:test:focal:bionic:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:bionic:i386
      artifacts: false

chroot-setup:test:focal:focal:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:focal:amd64
      artifacts: false

chroot-setup:test:focal:groovy:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:groovy:amd64
      artifacts: false

chroot-setup:test:focal:hirsute:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:focal
    - job: chroot-setup:build:hirsute:amd64
      artifacts: false

chroot-setup:deploy:
  stage: deploy
  image: docker:git
  services:
    - docker:dind
  variables:
    GIT_STRATEGY: none
  # Don't use `needs` here. Just use stages, so it only starts deployment when
  # all tests passed.
  #needs:
  #  - chroot-setup:test:bionic:bionic:amd64
  #  - ...
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
    - if [ -n "${DOCKER_USER}" ]; then
        cat "${DOCKER_PASS}" | base64 -d |
            docker login --username "${DOCKER_USER}" --password-stdin ${DOCKER_SERVER};
      fi

    - apk add --no-cache curl
    - curl --fail --show-error --location "https://github.com/genuinetools/reg/releases/download/v${REG_VERSION}/reg-linux-amd64" --output /usr/local/bin/reg
    - echo "${REG_SHA256}  /usr/local/bin/reg" | sha256sum -c -
    - chmod a+x /usr/local/bin/reg
  script:
    - |
      tags=$(/usr/local/bin/reg tags --auth-url "${CI_REGISTRY}" \
                 -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" \
                 "${CHROOT_SETUP_STAGING_PREFIX%:*}" | \
               grep "^${CHROOT_SETUP_STAGING_PREFIX##*:}")
      for tag in ${tags}; do
        which=$(echo "${tag}" | cut -d- -f3-);
        docker pull "${CHROOT_SETUP_STAGING_PREFIX}${which}";
        docker tag "${CHROOT_SETUP_STAGING_PREFIX}${which}" "${CHROOT_SETUP_TAG_PREFIX}${which}";
        docker push "${CHROOT_SETUP_TAG_PREFIX}${which}";
        docker rmi "${CHROOT_SETUP_TAG_PREFIX}${which}";

        if [ -n "${DOCKER_USER}" ]; then
          docker tag "${CHROOT_SETUP_STAGING_PREFIX}${which}" "${DOCKER_USER}/${DOCKER_REPO}:${which}";
          docker push "${DOCKER_USER}/${DOCKER_REPO}:${which}";
          docker rmi "${DOCKER_USER}/${DOCKER_REPO}:${which}";
        fi

        docker rmi "${CHROOT_SETUP_STAGING_PREFIX}${which}";
      done
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
